
// Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyAXWIqEZ4XeH3_PZ6eu8eG8WgojPhz_FRo",
    authDomain: "fir-66dac.firebaseapp.com",
    databaseURL: "https://fir-66dac.firebaseio.com",
    projectId: "fir-66dac",
    storageBucket: "fir-66dac.appspot.com",
    messagingSenderId: "358747289701",
    appId: "1:358747289701:web:0bd11fda57260367"
  };
  // Initialize Firebase
  const app = firebase.initializeApp(firebaseConfig);
    
  let database = app.database();
  let storage = app.storage();
  let storageRef = storage.ref();



 function Upload(file){
    let r = Math.random().toString(36).substring(7);

    var img = dataURLtoFile(file, r);
    

    var metadata = {
        contentType: 'image/jpeg',
        };

    let uploadTask = storageRef.child('images/'+r).put(img,metadata);
    uploadTask.on('state_changed', function(snapshot){
    let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    console.log('Upload is ' + progress + '% done');
    switch (snapshot.state) {
    case firebase.storage.TaskState.PAUSED: 
      console.log('Upload is paused');
      break;
    case firebase.storage.TaskState.RUNNING: 
      console.log('Upload is running');
      break;
  }
}, function(error) {
    console.log(error);
}, function() {
  uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
    console.log('File available at', downloadURL);

    firebase.database().ref('users/' + r).set({
      id:Date.now(),
      title: "original-"+r,
      imageURL : downloadURL
    });

  });
});
}

function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type:mime});
}

//Usage example:
